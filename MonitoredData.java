package assignment5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {
	private Date startTime;
	private Date endTime;
	private String activity;
	private static List<MonitoredData> data;	
	static Map<String,Integer> map = new HashMap<String,Integer>();
	
	public MonitoredData(Date startTime, String a, Date endTime,String b ,String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}

	public static void main(String [] args) { 
		String fileName = "E:\\An_2 Sem_2\\PT\\pt2020_30422_zburlea_danionut_assignment5\\Activities.txt";
		data = new ArrayList<MonitoredData>();
		DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		try(Stream<String> rows = Files.lines(Paths.get(fileName))){
			
			data = rows
				.map(line->line.split("\\t"))
				.map(obj-> {
					try {
						return new MonitoredData(dataFormat.parse(obj[0]), obj[1], dataFormat.parse(obj[2]), obj[3], obj[4]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					return null;
				})
				.collect(Collectors.toList());	
			
			rows.close();		
		} catch (IOException e) {
			e.printStackTrace();
		}
		Optional<MonitoredData> minData = data.stream()
									.collect(Collectors.minBy(Comparator.comparing(MonitoredData::getStartTime)));
		Optional<MonitoredData> maxData = data.stream()
										.collect(Collectors.maxBy(Comparator.comparing(MonitoredData::getEndTime)));
		
		int nrOfDaysTask2 = NumberOfDays(minData, maxData);
		try {
			printTask2(nrOfDaysTask2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Map<String,Long> mapTask3 = nrOfActivity();	
		try {
			printTask3(mapTask3);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Map<Integer,Map<String,Integer>> mapTask4 = activityPerDay();
		try {
			printTask4(mapTask4);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		data.forEach(a  -> PeriodOfLine(a));
		Map<String,Integer> mapTask5 = PeriodOfActiv();
		try {
			printTask5(mapTask5);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ArrayList<String> listTask6 = PeriodLessThan5(mapTask5);
		try {
			printTask6(listTask6);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void printTask2(int nrOfDays) throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter("Task_2.txt");
		printWriter.print("The number of days is: " + nrOfDays);
		printWriter.close();
	}
	
	public static void printTask3(Map<String, Long> map) throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter("Task_3.txt");
		for(Entry<String, Long> entry : map.entrySet()) {
            printWriter.print( entry.getKey() + ": " + entry.getValue() );
            printWriter.print("\n");
        }
		printWriter.close();
	}
	
	public static void printTask4(Map<Integer, Map<String, Integer>> map) throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter("Task_4.txt");
		for(Entry<Integer, Map<String, Integer>> entry : map.entrySet()) {
			for(Entry<String, Integer> entry1 : entry.getValue().entrySet()) {
				printWriter.print( "Day nr" + entry.getKey() + ": " + entry1.getKey() + ": " + entry1.getValue());
				printWriter.print("\n");
			}
        }
		printWriter.close();
	}
	
	public static void printTask5(Map<String, Integer> map) throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter("Task_5.txt");
		for(Entry<String, Integer> entry : map.entrySet()) {
            printWriter.print( entry.getKey() + ": " + entry.getValue() );
            printWriter.print("\n");
        }
		printWriter.close();
	}
	
	public static void printTask6(ArrayList<String> list) throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter("Task_6.txt");
		for(String string : list) {
            printWriter.print(string);
            printWriter.print("\n");
        }
		printWriter.close();
	}
	
	public static ArrayList<String> PeriodLessThan5(Map<String,Integer> map6){
		ArrayList<String> list = new ArrayList<String>();
		data.forEach(a->{
			String name = a.getActivity();
			if(!list.contains(name)) {
				if(PeriodOfLine(a) < 5) {
					list.add(name);
				}
			}
		});
		
		return list;
	}
	
	
	public static Map<String,Integer> PeriodOfActiv() {
		Map<String,Integer> map5=new HashMap<String,Integer>();
		
		data.forEach(a->{
						long val=0;
						String name=a.getActivity();
						
						if(map5.get(name)==null) {
							val=PeriodOfLine(a);
						}else {
							val=map5.get(name) + PeriodOfLine(a);

						}
						map5.put(name, (int) val);	
						
		});
		
		return map5;
	}
	
	@SuppressWarnings("deprecation")
	public static long PeriodOfLine(MonitoredData da) {
		
		long diff = 0;
		LocalDateTime start=LocalDateTime.of(da.getStartTime().getYear(), da.getStartTime().getMonth(), da.getStartTime().getDate(),da.getStartTime().getHours(),da.getStartTime().getMinutes(),da.getStartTime().getSeconds());
		LocalDateTime stop=LocalDateTime.of(da.getEndTime().getYear(), da.getEndTime().getMonth(), da.getEndTime().getDate(),da.getEndTime().getHours(),da.getEndTime().getMinutes(),da.getEndTime().getSeconds());
		diff=Math.abs(Duration.between(stop, start).toMinutes());
		return diff;
		
	}
	

	@SuppressWarnings("deprecation")
	public static Map<Integer,Map<String,Integer>> activityPerDay() {
		Map<Integer,Map<String,Integer>> map4= new HashMap<Integer,Map<String,Integer>>();
		
		data.forEach(a -> {
						int day = a.getStartTime().getDate();
						
						if(map4.get(day) == null) {
							map = new HashMap<String,Integer>();
							map4.put(day, map);
						}
						String activity = a.getActivity();
						int val;
						
						if(map.get(activity) == null) {
							val = 0;
						}else {
							val = map.get(activity);
						}
						map4.get(day).put(activity, ++val);
						
					});
		return map4;
	}
	
	public static Map<String,Long> nrOfActivity() {
		Map<String,Long> map=new HashMap<String,Long>();
		
		long nrSleep=data.stream()
						  .filter(a->a.getActivity().equals("Sleeping"))
						  .count();
		map.put("Sleeping", nrSleep);
		
		long nrToileting=data.stream()
				  			  .filter(a->a.getActivity().equals("Toileting"))
				  			  .count();
		map.put("Toileting", nrToileting);
		
		long nrShowering=data.stream()
	  			  				.filter(a->a.getActivity().equals("Showering"))
	  			  				.count();
		
		map.put("Showering", nrShowering);
		
		long nrBreakfast=data.stream()
			  				.filter(a->a.getActivity().equals("Breakfast"))
			  				.count();
		
		map.put("Breakfast", nrBreakfast);
		
		long nrGrooming=data.stream()
			  				.filter(a->a.getActivity().equals("Grooming"))
			  				.count();
		
		map.put("Grooming", nrGrooming);

		long nrSpare_Time=data.stream()
			.filter(a->a.getActivity().equals("Spare_Time/TV"))
			.count();
		
		map.put("Spare_Time/TV", nrSpare_Time);
		
		long nrLeaving=data.stream()
		  				.filter(a->a.getActivity().equals("Leaving"))
		  				.count();
		
		map.put("Leaving", nrLeaving);
		
		long nrLunch=data.stream()
		  				.filter(a->a.getActivity().equals("Lunch"))
		  				.count();
		
		map.put("Lunch", nrLunch);
		
		long nrSnack=data.stream()
		  				.filter(a->a.getActivity().equals("Snack"))
		  				.count();
		
		map.put("Snack", nrSnack);
		
		long nrDinner=data.stream()
  				.filter(a->a.getActivity().equals("Dinner"))
  				.count();

		map.put("Dinner", nrDinner);
		
		return map;
	}
	
	@SuppressWarnings("deprecation")
	public static int NumberOfDays(Optional<MonitoredData> minData,Optional<MonitoredData> maxData) {
		int count=0;
		int mounth1=minData.get().getStartTime().getMonth() + 1;
		int mounth2=maxData.get().getEndTime().getMonth() + 1;	
		int day1= minData.get().getStartTime().getDate();
		int day2= maxData.get().getEndTime().getDate();
		if(mounth1==mounth2) {
			if(day1>day2) {
				count=day1-day2;
			}else {
				count=day2-day1;
			}
		}else 
			while(mounth1<mounth2) {
				int aux=1;
				int auxD=day1;
				
				while(auxD < 31) {
					auxD++;
					count++;
				}
				while(aux<=day2) {
					count++;
					aux++;
				}
				mounth1++;
			}
		return count;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public String getActivity() {
		return activity;
	}
	
	public String toString() {
		return "startTime=" + startTime + ", endTime=" + endTime + ", activity=" + activity ;
	}
	
}
